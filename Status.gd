extends Control

#Temp delete
#onready var player = get_parent().get_parent()
onready var player = Global.player.get_node("Ball")
onready var direction = $Control/direction
onready var velocity = $Control/velocity
onready var mesh = $Control/mesh

func _physics_process(delta):
	#var h_rot = get_parent().get_node("CameraGimbal/InnerGimbal").global_transform.basis.get_euler().y
	var h_rot = Global.player.get_node("CameraGimbal/InnerGimbal").global_transform.basis.get_euler().y
	$Control.set_rotation(h_rot)
	direction.rotation = atan2(-player.direction.z, -player.direction.x)
	#velocity.position = Vector2(-player.velocity.x, -player.velocity.z) * 10
	#velocity.position = Vector2(-player.get_linear_velocity().x, -player.get_linear_velocity().z) * 10
	#velocity = Global.ball_speed
	velocity.position = Vector2(0,-abs(Global.ball_speed))*5
#	mesh.rotation_degrees = -90-get_node("../Mesh").rotation_degrees.y - player.rotation_degrees.y
	pass
	
func _input(event):
	if event is InputEventKey: #checking which buttons are being pressed
		if event.as_text() == "W" || event.as_text() == "A" || event.as_text() == "S" || event.as_text() == "D" || event.as_text() == "Space":
			if event.pressed:
				get_node(event.as_text()).color = Color("ff6666")
			else:
				get_node(event.as_text()).color = Color("ffffff")
