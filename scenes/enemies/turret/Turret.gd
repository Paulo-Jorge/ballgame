extends Spatial

#export (PackedScene) var Bullet
var Bullet = preload("res://scenes/enemies/turret/Bullet.tscn")

onready var mesh = get_node("Mesh")
var shoot_interval = 1.0#seconds
var active = false

# Create a timer node
var bullet_timer = Timer.new()

# Called when the node enters the scene tree for the first time.
func _ready():
	# Set timer interval
	bullet_timer.set_wait_time(shoot_interval)
	# Set it as repeat
	bullet_timer.set_one_shot(false)
	# Connect its timeout signal to the function you want to repeat
	bullet_timer.connect("timeout", self, "shoot")
	# Add to the tree as child of the current node
	add_child(bullet_timer)
	#To start:
	#bullet_timer.start()


func _process(delta):
	mesh.look_at(Global.player.get_node("Ball").global_transform.origin,Vector3(0,1,0))
	#if active == true:
		#yield( get_tree().create_timer(2), "timeout" )
		#shoot()
		#pass
		

func _on_Area_body_entered(body):
	if body.name == "Ball":
		active = true
		shoot()
		bullet_timer.start()


#func _physics_process(delta):
#	velocity += g * delta
#	look_at(transform.origin + velocity.normalized(), Vector3.UP)
#	transform.origin += velocity * delta


func _on_Area_body_exited(body):
	if body.name == "Ball":
		active = false
		bullet_timer.stop()


func shoot():
	var b = Bullet.instance()
	owner.add_child(b)
	b.transform = $Mesh/Gun/Fire.global_transform
	b.velocity = -b.transform.basis.z * b.muzzle_velocity
