extends Area

#signal exploded

export var muzzle_velocity = 15#25
export var g = Vector3.DOWN * 20
var velocity = Vector3.ZERO
var max_distance = 30
var distance_traveled = Vector3()

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func _physics_process(delta):
	#This line adds gravity to the bullet
	#velocity += g * delta
	# Update bullet mesh to face it's travel movement forward
	look_at(transform.origin + velocity.normalized(), Vector3.UP)
	transform.origin += velocity * delta
	#distance_traveled += velocity * delta

	#After x travel distance delete
	if self.global_transform.origin.distance_to(get_parent().get_node("Turret/Mesh").global_transform.origin) > max_distance:
		queue_free()


#func _on_Shell_body_entered(body):
#    emit_signal("exploded", transform.origin)
#    queue_free()
