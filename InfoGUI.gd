extends Control

onready var fps = get_node("ColorRect/GridContainer/fps")
onready var velocity = get_node("ColorRect/GridContainer/velocity_value")
onready var camera = get_node("ColorRect/GridContainer/camera")
#onready var player = 

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	#var fps_txt = ""
	#fps_txt += "fps: " + str(Engine.get_frames_per_second())
	#fps_txt += "FPS: " + str(fps.variable)
	#fps.text = str(Engine.get_frames_per_second())
	pass

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	fps.text = str(Engine.get_frames_per_second())
	velocity.text = str(Global.ball_speed)
	camera.text = str(Global.cam_type)
