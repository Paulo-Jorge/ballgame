extends RigidBody

#temp
var direction = Vector3.BACK

var key_press = {}

#Ball
#export var rotate_speed = 1#10
var rotate_speed = Global.powerup_speed
export var max_rotate = 25
export var temp_angular_velocity = Vector3(0,0,0)
export var sfx1_top_volume = 10
export var sfx1_min_volume = -90
export var jump_force = 10

#Camera
#Camera movemente inspired by: http://kidscancode.org/godot_recipes/3d/camera_gimbal/
onready var camera_gimbal = get_parent().get_node("CameraGimbal")#move only in y axis
onready var inner_gimbal = get_parent().get_node("CameraGimbal/InnerGimbal")#move only in x axis
onready var camera = get_parent().get_node("CameraGimbal/InnerGimbal/Camera")
export var camera_type = "joystick" #joystick / behind / fixed / joystick_locked / behind_locked

export var camera_distance = 7.0
export var camera_rotation = -25.0
export var cam_rotation_speed = 300.0
export var camera_up = Vector3(0,1,0)
#apagar
export var camera_height = 3.0







onready var raycast_floor = get_parent().get_node("RayCast_floor")
onready var raycast_camera_r = get_parent().get_node("CameraGimbal/InnerGimbal/Camera/RayCast_Camera_R")
onready var raycast_camera_l = get_parent().get_node("CameraGimbal/InnerGimbal/Camera/RayCast_Camera_L")

#Music
onready var sfx1 = get_node("SFX1")

var play_sfx_floor = false

# not sure what to put here to get proper turning
#warning-ignore:unused_class_variable
#var turn_speed = Vector3(8,0,0)

# not sure what to put here to get proper movement
#warning-ignore:unused_class_variable
#var movement_speed = Vector3(5,0,0)

# Called when the node enters the scene tree for the first time.
func _ready():
	#var h_rot = Global.player.get_node("CameraGimbal/InnerGimbal").global_transform.basis.get_euler().y
	#direction = direction.rotated(Vector3.UP, h_rot).normalized()
	#direction = Vector3.BACK.rotated(Vector3.UP, $CameraGimbal/InnerGimbal.global_transform.basis.get_euler().y)
	#direction = Vector3.BACK.rotated(Vector3.UP, h_rot)
	camera.transform.origin.z = camera_distance
	inner_gimbal.rotation_degrees.x = camera_rotation
	#sfx1.play()
	#camera.transform.origin.z -= 20
	
#	camera.global_transform.origin = global_transform.origin
#	camera.global_transform.basis = global_transform.basis
#	camera.global_transform.origin -= Vector3(4,0,0)

	#camera.global_transform.origin.z += camera_distance
	#camera.transform.origin.z += camera_distance
	
	set_physics_process(true)
	#pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

#Find highest value in a list and convert negative to positve
func _max(list):
	var ret
	for val in list:
		if not ret:
			ret = abs(val)
		else:
			if abs(val) > ret:
				ret = abs(val)
	return ret

func active_powers():
	if Global.powerup_shield > 1:
		var shield = get_parent().get_node("Powers/Shield")
		var temp_dir = self.linear_velocity.normalized()
		### 0.1 = distância... Mais = + distante. Não descobri como controlar com precisão
		shield.global_transform.origin = self.global_transform.origin + temp_dir * 0.1
		shield.look_at(self.global_transform.origin,Vector3(0,1,0))

#backup of the 1st movement logic = more control, less realism
func control_fixed():
	if Input.is_action_pressed("move_left"):
		temp_angular_velocity.x = rotate_speed
	elif Input.is_action_pressed("move_right"):
		temp_angular_velocity.x = -rotate_speed
	else:
		temp_angular_velocity.x = 0
	if Input.is_action_pressed("move_forward"):
		temp_angular_velocity.z = rotate_speed
	elif Input.is_action_pressed("move_backward"):
		temp_angular_velocity.z = -rotate_speed
	else:
		temp_angular_velocity.z = 0
	
#func _input(event):
#	pass

#export var distance = 5.0
#export var height = 2.0
#export var up = Vector3(0,1,0)
func camera_raycast():
	var player = self
	#print(player.global_transform.origin)
	#get_trasnform
	#var player_direction = player.get_linear_velocity().normalized()
	var player_direction = player.get_linear_velocity()
	
	#print(player_direction)
	var target = player.global_transform
	var target_pos = target.origin
	#var cam_pos = target_pos
#	cam_pos.x += + distance
#	cam_pos.y = target_pos.y + height
#	camera.look_at_from_position(cam_pos, target_pos, up)
	
	if camera_type == "fixed":
		camera_gimbal.global_transform.origin.z = target_pos.z + camera_distance
		camera_gimbal.global_transform.origin.y = target_pos.y + camera_height
		camera_gimbal.global_transform.origin.x = target_pos.x
	elif camera_type == "behind":
		var temp_dir = self.linear_velocity.normalized()
		### 0.1 = distância... Mais = + distante. Não descobri como controlar com precisão
		camera_gimbal.global_transform.origin = self.global_transform.origin
		var angle = temp_dir.dot(Vector3(-1,0,0))
		camera_gimbal.rotation.y = angle

		
		#camera_gimbal.look_at(self.global_transform.origin,Vector3(0,1,0))

		#camera_gimbal.global_transform.origin.z = target_pos.z + camera_distance
		#camera_gimbal.global_transform.origin.y = target_pos.y + camera_height
		#camera_gimbal.global_transform.origin.x = target_pos.x
		
	#elif camera_type == "behind" or camera_type == "behind_locked":
	elif camera_type == "behind_locked":
		camera_gimbal.global_transform.origin.z = target_pos.z
		camera_gimbal.global_transform.origin.y = target_pos.y
		camera_gimbal.global_transform.origin.x = target_pos.x
		
		var temp = target.basis.z
		temp.y = 0
		temp.x = 0
		
		#camera_gimbal.rotation.y = camera_gimbal.global_transform.basis.z.angle_to(target.basis.z)

		#camera_gimbal.rotation_degrees.y -= player_direction.normalized().dot(camera_gimbal.transform.basis.z)
		
		camera_gimbal.transform.basis.z.z = player_direction.normalized().z * -1
		camera_gimbal.transform.basis.z.x = player_direction.normalized().x * -1

		camera_gimbal.transform.basis.x.z = player_direction.normalized().x * -1
		camera_gimbal.transform.basis.x.x = player_direction.normalized().z * -1
		
		#camera_gimbal.rotation_degrees.y -= 20
		#print(player_direction.normalized().dot(camera_gimbal.transform.basis.z))
		#print(camera_gimbal.transform.basis.x, camera_gimbal.transform.basis.z)
		#camera_gimbal.rotation.y = player_direction.norm4alized().y
		
		#print(camera_gimbal.global_transform.basis.z.angle_to(target.basis.z))
		#print(get_transform().basis.z * -1)
		#print((player.global_position - camera_gimbal.global_position).angle())
		
		#TEORIA:
		#frente da ball
		#global_transform.basis.z.z = "inclinação"
		#global_transform.origin = "centro do circulo"
		#distancia da cam = raio
		#came_rotation_degrees atual = ponto atual: calcular quantos degress para a 1º var inclinação e somar
		
		#camera_gimbal.rotation.y = player_direction.y

		#(player_direction - camera_gimbal.transform.origin).normalized().y
		
		#Direction retirado da net:
		#(player.transform.origin - transform.origin).normalized()
		
#		camera.global_transform.origin.z = player_direction.z + camera_distance
#		camera.global_transform.origin.y = player_direction.y + camera_height
#		camera.global_transform.origin.x = player_direction.x
#		camera.global_transform.origin.z = target_pos.z - player_direction.z * camera_distance
#		camera.global_transform.origin.y = target_pos.y + camera_height
#		camera.global_transform.origin.x = target_pos.x - player_direction.x * camera_distance
	elif camera_type == "joystick" or camera_type == "joystick_locked":
		camera_gimbal.global_transform.origin.z = target_pos.z
		camera_gimbal.global_transform.origin.y = target_pos.y
		camera_gimbal.global_transform.origin.x = target_pos.x
	
	###Move camera with velocity effect
#	var temp_velocity = get_linear_velocity()
#	var highest_velocity = _max([temp_velocity.x, temp_velocity.y, temp_velocity.z])
#	if highest_velocity > 0:
#		camera.global_transform.origin.z = camera.global_transform.origin.z + (highest_velocity * 0.05)
#		camera.global_transform.origin.y = camera.global_transform.origin.y + (highest_velocity * 0.05)
	
	camera.look_at(target_pos,camera_up)
	
	#Raycast update
	raycast_floor.global_transform.origin.z = target_pos.z
	raycast_floor.global_transform.origin.y = target_pos.y
	raycast_floor.global_transform.origin.x = target_pos.x
	
	#camera.global_transform.origin = obj.origin
#	camera.global_transform.basis.z = Vector3(0,0,0)
#	camera.global_transform.basis.y = Vector3(0,0,0)
	#camera.look_at(obj.origin, Vector3(0.0, 1.0, 0.0))
	
	
#	camera.global_transform.basis.y = Vector3(0,1,0)
#	camera.global_transform.basis.z = Vector3(0,0,1)
	
	#camera.global_transform.origin -= Vector3(4,0,0)
	#camera.transform.origin = transform.origin
	#camera.transform.basis = transform.basis
	#print(global_transform.origin.normalized())
	#pass


	#Debug / Direction / APAGAR
	var dir_mesh = get_parent().get_node("Direction")
	var temp_dir = player.linear_velocity.normalized()
	dir_mesh.global_transform.origin = target.origin + temp_dir
	#dir_mesh.global_transform.origin.z += 3
	#dir_mesh.global_transform.origin = target.origin

	#dir_mesh.look_at(target.origin,Vector3(0,1,0))
	
	

func _physics_process(delta):

	#if Input.is_action_pressed("camera_right") and camera_type == "joystick":
		#camera.global_transform.origin.x += 2 * delta

	#esytava no fim, mas mudei para aqui para testar pois a doc acho que aconselha o raycast aqui
	camera_raycast()
	active_powers()
	#camera()
#	var isOnFloor = is_on_floor()
#	print(isOnFloor)

#	if contact_monitor():
#		print(5)

	#FLOOR SOUND SFX
	
	
	#if get_colliding_bodies():
		#var collisions = get_colliding_bodies()
		#print(collisions[0].get_name())
#		for body in get_colliding_bodies():
#			print(body[0].get_name())

		
	if raycast_floor.is_colliding():
		var temp_velocity = get_linear_velocity()
		#Find the highest one in the x y z Vector
		var highest_velocity = _max([temp_velocity.x, temp_velocity.y, temp_velocity.z])
		
		if highest_velocity>0:
			var new_volume = sfx1_min_volume + highest_velocity * 10
			if new_volume > sfx1_top_volume:
				new_volume = sfx1_top_volume
			AudioServer.set_bus_volume_db(1, new_volume)
#			if highest_velocity >= 3:
#				AudioServer.set_bus_volume_db(1, 0)
#			if highest_velocity < 3:
#				AudioServer.set_bus_volume_db(1, -10)
#			if highest_velocity < 2:
#				AudioServer.set_bus_volume_db(1, -15)
#			if highest_velocity < 1:
#				AudioServer.set_bus_volume_db(1, -20)

			#for body in get_colliding_bodies():
			if play_sfx_floor == false:
				sfx1.play()
				play_sfx_floor = true
		else:
			sfx1.stop()
			play_sfx_floor = false
	else:
		sfx1.stop()
		play_sfx_floor = false



# CAM MOVES WITH MOUSE CLICK LIKE FPS - DISABEL FOLLOW CAM 1ST ABOVE
#var rot_x = 0
#var rot_y = 0
#func _input(event):
#	if event is InputEventMouseMotion and event.button_mask & 1:
#		print(555)
#		# modify accumulated mouse rotation
#		rot_x += event.relative.x * 0.01
#		rot_y += event.relative.y * 0.01
#		camera.transform.basis = Basis() # reset rotation
#		camera.rotate_object_local(Vector3(0, 1, 0), rot_x) # first rotate in Y
#		camera.rotate_object_local(Vector3(1, 0, 0), rot_y) # then rotate in X


#func _process(delta):
#	#CAMERA TYPE
#    if Input.is_action_pressed("camera_1"):
#        camera_type = "joystick"
#    elif Input.is_action_pressed("camera_2"):
#        camera_type = "behind"
#    elif Input.is_action_pressed("camera_3"):
#        camera_type = "fixed"
#    elif Input.is_action_pressed("camera_4"):
#        camera_type = "joystick_locked"
#    elif Input.is_action_pressed("camera_5"):
#        camera_type = "behind_locked"

func _integrate_forces(state):
	#CAMERA TYPE
	if Input.is_action_pressed("camera_1"):
		camera_type = "joystick"
	elif Input.is_action_pressed("camera_2"):
		camera_type = "behind"
	elif Input.is_action_pressed("camera_3"):
		camera_type = "fixed"
	elif Input.is_action_pressed("camera_4"):
		camera_type = "joystick_locked"
	elif Input.is_action_pressed("camera_5"):
		camera_type = "behind_locked"
	
	#CAMERA ROTATE
	if Input.is_action_pressed("camera_right") and (camera_type == "joystick" or camera_type == "joystick_locked"):
		if raycast_camera_r.is_colliding():
			pass
		else:
			camera_gimbal.rotation_degrees.y += cam_rotation_speed * get_physics_process_delta_time()
	elif Input.is_action_pressed("camera_left") and (camera_type == "joystick" or camera_type == "joystick_locked"):
		if raycast_camera_l.is_colliding():
			pass
		else:
			camera_gimbal.rotation_degrees.y -= cam_rotation_speed * get_physics_process_delta_time()
	if Input.is_action_pressed("camera_up") and (camera_type == "joystick" or camera_type == "joystick_locked"):
		var x = cam_rotation_speed * get_physics_process_delta_time()
		inner_gimbal.rotation_degrees.x = clamp(inner_gimbal.rotation_degrees.x - x, -89.0, 1.0)
	elif Input.is_action_pressed("camera_down") and (camera_type == "joystick" or camera_type == "joystick_locked"):
		var x = cam_rotation_speed * get_physics_process_delta_time()
		inner_gimbal.rotation_degrees.x = clamp(inner_gimbal.rotation_degrees.x + x, -89.0, 1.0)
	
	#BAL MOVEMENT
	temp_angular_velocity = state.angular_velocity
	
	var h_rot = Global.player.get_node("CameraGimbal/InnerGimbal").global_transform.basis.get_euler().y
	#direction = direction.rotated(Vector3.UP, h_rot).normalized()
	direction = Global.player.get_node("CameraGimbal/InnerGimbal").global_transform.basis.z
	#print(direction)
	
	if camera_type == "behind_locked" or camera_type == "joystick_locked":
		#Register key press for movement, so we can store values at press time
		#So when camera rotates, the key pressed don't change imediatily relative to the camera
		if Input.is_action_pressed("move_left") and key_press.has("move_left") == false:
			key_press["move_left"] = camera.global_transform.basis.z
		elif Input.is_action_just_released("move_left"):
			key_press.erase("move_left")
		if Input.is_action_pressed("move_right") and key_press.has("move_right") == false:
			key_press["move_right"] = camera.global_transform.basis.z
		elif Input.is_action_just_released("move_right"):
			key_press.erase("move_right")
		if Input.is_action_pressed("move_forward") and key_press.has("move_forward") == false:
			key_press["move_forward"] = camera.global_transform.basis.x
		elif Input.is_action_just_released("move_forward"):
			key_press.erase("move_forward")
		if Input.is_action_pressed("move_backward") and key_press.has("move_backward") == false:
			key_press["move_backward"] = camera.global_transform.basis.x
		elif Input.is_action_just_released("move_backward"):
			key_press.erase("move_backward")
		
		#START MOV INPUT
		if key_press.empty() != true:
			if camera_type == "behind_locked" or camera_type == "joystick_locked":
				if key_press.has("move_left") == true:
					var temp_vector = temp_angular_velocity + key_press["move_left"] * rotate_speed
					#Reset Y, we dont want the camera to influence it:
					temp_vector.y = temp_angular_velocity.y
					#Maintain equal some vars if x happens (f.e. don't let go above max_speed etc)
					if temp_vector.x > temp_angular_velocity.x and temp_vector.x >= max_rotate:
						temp_vector.x = temp_angular_velocity.x
					elif temp_vector.x < temp_angular_velocity.x and temp_vector.x <= -max_rotate:
						temp_vector.x = temp_angular_velocity.x
					if temp_vector.z > temp_angular_velocity.z and temp_vector.z >= max_rotate:
						temp_vector.z = temp_angular_velocity.z
					elif temp_vector.z < temp_angular_velocity.z and temp_vector.z <= -max_rotate:
						temp_vector.z = temp_angular_velocity.z
					temp_angular_velocity = temp_vector
				if key_press.has("move_right") == true:
					var temp_vector = temp_angular_velocity - key_press["move_right"] * rotate_speed
					temp_vector.y = temp_angular_velocity.y
					if temp_vector.x > temp_angular_velocity.x and temp_vector.x >= max_rotate:
						temp_vector.x = temp_angular_velocity.x
					elif temp_vector.x < temp_angular_velocity.x and temp_vector.x <= -max_rotate:
						temp_vector.x = temp_angular_velocity.x
					if temp_vector.z > temp_angular_velocity.z and temp_vector.z >= max_rotate:
						temp_vector.z = temp_angular_velocity.z
					elif temp_vector.z < temp_angular_velocity.z and temp_vector.z <= -max_rotate:
						temp_vector.z = temp_angular_velocity.z
					temp_angular_velocity = temp_vector
				if key_press.has("move_backward") == true:
					var temp_vector = temp_angular_velocity + key_press["move_backward"] * rotate_speed
					temp_vector.y = temp_angular_velocity.y
					if temp_vector.x > temp_angular_velocity.x and temp_vector.x >= max_rotate:
						temp_vector.x = temp_angular_velocity.x
					elif temp_vector.x < temp_angular_velocity.x and temp_vector.x <= -max_rotate:
						temp_vector.x = temp_angular_velocity.x
					if temp_vector.z > temp_angular_velocity.z and temp_vector.z >= max_rotate:
						temp_vector.z = temp_angular_velocity.z
					elif temp_vector.z < temp_angular_velocity.z and temp_vector.z <= -max_rotate:
						temp_vector.z = temp_angular_velocity.z
					temp_angular_velocity = temp_vector
				if key_press.has("move_forward") == true:
					var temp_vector = temp_angular_velocity - key_press["move_forward"] * rotate_speed
					temp_vector.y = temp_angular_velocity.y
					if temp_vector.x > temp_angular_velocity.x and temp_vector.x >= max_rotate:
						temp_vector.x = temp_angular_velocity.x
					elif temp_vector.x < temp_angular_velocity.x and temp_vector.x <= -max_rotate:
						temp_vector.x = temp_angular_velocity.x
					if temp_vector.z > temp_angular_velocity.z and temp_vector.z >= max_rotate:
						temp_vector.z = temp_angular_velocity.z
					elif temp_vector.z < temp_angular_velocity.z and temp_vector.z <= -max_rotate:
						temp_vector.z = temp_angular_velocity.z
					temp_angular_velocity = temp_vector
	
			state.set_angular_velocity(temp_angular_velocity)
	
		if Input.is_action_pressed("jump"):
			if Global.powerup_jump > 0:
				if raycast_floor.is_colliding():
					apply_central_impulse(Vector3(0,jump_force,0))
				
				
				
				
	#END if camera_type == "behind_locked" or camera_type == "joystick_locked"
	
		
	if camera_type == "behind" or camera_type == "joystick":
		if Input.is_action_pressed("move_left"):
			var temp_vector = temp_angular_velocity + camera.global_transform.basis.z * rotate_speed
			#Reset Y, we dont want the camera to influence it
			temp_vector.y = temp_angular_velocity.y
			if temp_vector.x > temp_angular_velocity.x and temp_vector.x >= max_rotate:
				temp_vector.x = temp_angular_velocity.x
			elif temp_vector.x < temp_angular_velocity.x and temp_vector.x <= -max_rotate:
				temp_vector.x = temp_angular_velocity.x
			if temp_vector.z > temp_angular_velocity.z and temp_vector.z >= max_rotate:
				temp_vector.z = temp_angular_velocity.z
			elif temp_vector.z < temp_angular_velocity.z and temp_vector.z <= -max_rotate:
				temp_vector.z = temp_angular_velocity.z
			temp_angular_velocity = temp_vector
		if Input.is_action_pressed("move_right"):
			var temp_vector = temp_angular_velocity - camera.global_transform.basis.z * rotate_speed
			temp_vector.y = temp_angular_velocity.y
			if temp_vector.x > temp_angular_velocity.x and temp_vector.x >= max_rotate:
				temp_vector.x = temp_angular_velocity.x
			elif temp_vector.x < temp_angular_velocity.x and temp_vector.x <= -max_rotate:
				temp_vector.x = temp_angular_velocity.x
			if temp_vector.z > temp_angular_velocity.z and temp_vector.z >= max_rotate:
				temp_vector.z = temp_angular_velocity.z
			elif temp_vector.z < temp_angular_velocity.z and temp_vector.z <= -max_rotate:
				temp_vector.z = temp_angular_velocity.z
			temp_angular_velocity = temp_vector
		if Input.is_action_pressed("move_backward"):
			var temp_vector = temp_angular_velocity + camera.global_transform.basis.x * rotate_speed
			temp_vector.y = temp_angular_velocity.y
			if temp_vector.x > temp_angular_velocity.x and temp_vector.x >= max_rotate:
				temp_vector.x = temp_angular_velocity.x
			elif temp_vector.x < temp_angular_velocity.x and temp_vector.x <= -max_rotate:
				temp_vector.x = temp_angular_velocity.x
			if temp_vector.z > temp_angular_velocity.z and temp_vector.z >= max_rotate:
				temp_vector.z = temp_angular_velocity.z
			elif temp_vector.z < temp_angular_velocity.z and temp_vector.z <= -max_rotate:
				temp_vector.z = temp_angular_velocity.z
			temp_angular_velocity = temp_vector
		if Input.is_action_pressed("move_forward"):
			var temp_vector = temp_angular_velocity - camera.global_transform.basis.x * rotate_speed
			temp_vector.y = temp_angular_velocity.y
			if temp_vector.x > temp_angular_velocity.x and temp_vector.x >= max_rotate:
				temp_vector.x = temp_angular_velocity.x
			elif temp_vector.x < temp_angular_velocity.x and temp_vector.x <= -max_rotate:
				temp_vector.x = temp_angular_velocity.x
			if temp_vector.z > temp_angular_velocity.z and temp_vector.z >= max_rotate:
				temp_vector.z = temp_angular_velocity.z
			elif temp_vector.z < temp_angular_velocity.z and temp_vector.z <= -max_rotate:
				temp_vector.z = temp_angular_velocity.z
			temp_angular_velocity = temp_vector
			
		if Input.is_action_pressed("jump"):
			if Global.powerup_jump > 0:
				if raycast_floor.is_colliding():
					apply_central_impulse(Vector3(0,jump_force,0))

		state.set_angular_velocity(temp_angular_velocity)
		
	#FIXED MOVEMENT
	elif camera_type == "fixed":
		if Input.is_action_pressed("move_left") and temp_angular_velocity.z <= max_rotate:
			temp_angular_velocity.z += rotate_speed
		elif Input.is_action_pressed("move_right") and temp_angular_velocity.z >= -max_rotate:
			temp_angular_velocity.z -= rotate_speed
		if Input.is_action_pressed("move_backward") and temp_angular_velocity.x <= max_rotate:
			temp_angular_velocity.x += rotate_speed
		elif Input.is_action_pressed("move_forward") and temp_angular_velocity.x >= -max_rotate:
			if camera_type == "joystick":
				temp_angular_velocity -= camera.global_transform.basis.x * 0.1
			else:
				temp_angular_velocity.x -= rotate_speed
#
	if Input.is_action_pressed("jump"):
		if Global.powerup_jump > 0:
			if raycast_floor.is_colliding():
				apply_central_impulse(Vector3(0,jump_force,0))

	state.set_angular_velocity(temp_angular_velocity)


	#GLOBAL GUI SAVE
	save_in_global(state)

#Save important vars in the Global
func save_in_global(player):
	#Ball speed
	#player.get_linear_velocity()
	var speed_x = player.get_linear_velocity().x
	var speed_y = player.get_linear_velocity().z
	if abs(player.get_linear_velocity().x) > abs(player.get_linear_velocity().z):
		Global.ball_speed = int(abs(player.get_linear_velocity().x))
	else:
		Global.ball_speed = int(abs(player.get_linear_velocity().z))
	Global.cam_type = camera_type
	#Global.ball_speed = player.get_linear_velocity()[player.get_linear_velocity().abs().max_axis()]

	#camera_raycast()
#_physics_process

#func _input(event):
#    if is_on_floor():
#        pass

#if is_on_floor():
#	print(5)

#func _on_Ball_body_entered(body):
	#if body == player:
	#print(5)
