extends RigidBody

#Ball
export var rotate_speed = 1#10
export var max_rotate = 10
export var temp_angular_velocity = Vector3(0,0,0)

#Camera
export var camera_distance = 4.0
export var camera_height = 3.0

onready var camera = get_node("/root/Camera")

# not sure what to put here to get proper turning
#warning-ignore:unused_class_variable
#var turn_speed = Vector3(8,0,0)

# not sure what to put here to get proper movement
#warning-ignore:unused_class_variable
#var movement_speed = Vector3(5,0,0)

# Called when the node enters the scene tree for the first time.
func _ready():
	#camera.transform.origin.z += 20
	#camera.transform.basis = transform.basis
	set_physics_process(true)
	#pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

#backup of the 1st movement logic = more control, less realism
func control_fixed():
	if Input.is_action_pressed("move_left"):
		temp_angular_velocity.x = rotate_speed
	elif Input.is_action_pressed("move_right"):
		temp_angular_velocity.x = -rotate_speed
	else:
		temp_angular_velocity.x = 0
	if Input.is_action_pressed("move_forward"):
		temp_angular_velocity.z = rotate_speed
	elif Input.is_action_pressed("move_backward"):
		temp_angular_velocity.z = -rotate_speed
	else:
		temp_angular_velocity.z = 0
	
#func _input(event):
#	pass

#func _physics_process(delta):
func _integrate_forces(state):
	#print(camera.global_transform.basis)
	temp_angular_velocity = state.angular_velocity
	if Input.is_action_pressed("move_left") and temp_angular_velocity.x <= max_rotate:
		#temp_angular_velocity.x = rotate_speed
		temp_angular_velocity.x += rotate_speed
	elif Input.is_action_pressed("move_right") and temp_angular_velocity.x >= -max_rotate:
		#temp_angular_velocity.x = -rotate_speed
		temp_angular_velocity.x -= rotate_speed
#	else:
#		temp_angular_velocity.x = 0
	if Input.is_action_pressed("move_forward") and temp_angular_velocity.z <= max_rotate:
		#temp_angular_velocity.z = rotate_speed
		temp_angular_velocity.z += rotate_speed
	elif Input.is_action_pressed("move_backward") and temp_angular_velocity.z >= -max_rotate:
		temp_angular_velocity.z -= rotate_speed
		#temp_angular_velocity.z = -rotate_speed
#	else:
#		temp_angular_velocity.z = 0
		
		
	#if Input.is_action_pressed("move_left"):
		#state.set_angular_velocity(-turn_speed)
	#	state.set_angular_velocity(Vector3(rotate_speed,0,0))
	#if Input.is_action_pressed("move_right"):
		#state.set_angular_velocity(turn_speed)
	#	state.set_angular_velocity(Vector3(-rotate_speed,0,0))
	#if Input.is_action_pressed("move_forward"):
	#	state.set_angular_velocity(Vector3(0,0,rotate_speed))
		#state.set_linear_velocity(movement_speed)
	#if Input.is_action_pressed("move_backward"):
	#	state.set_angular_velocity(Vector3(0,0,-rotate_speed))
		#state.set_linear_velocity(-movement_speed)
		
	state.set_angular_velocity(temp_angular_velocity)
		
#_physics_process


#if is_on_floor:
	#sound
