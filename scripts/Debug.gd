extends Control

onready var player = Global.player.get_node("Ball")
onready var camera = Global.player.get_node("CameraGimbal/InnerGimbal/Camera")
var width = 5.0

func _draw():
	var color = Color(0, 1, 0)
	var start = camera.unproject_position(player.global_transform.origin)
	var end = camera.unproject_position(player.global_transform.origin + player.linear_velocity.normalized())
	draw_line(start, end, color, width)
	draw_triangle(end, start.direction_to(end), width*2, color)

func draw_triangle(pos, dir, size, color):
	var a = pos + dir * size
	var b = pos + dir.rotated(2*PI/3) * size
	var c = pos + dir.rotated(4*PI/3) * size
	var points = PoolVector2Array([a, b, c])
	draw_polygon(points, PoolColorArray([color]))
	
func _process(delta):
	update()

