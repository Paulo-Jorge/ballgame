extends Node

var current_scene = "Game"
var current_level = "1"

#Runtime vars
var ball_speed = 0#used by the gui panel
#var ball_speed_x = 0
#var ball_speed_z = 0
var cam_type = ""

#Important Nodes
onready var player = get_tree().get_root().get_node("World/Player")
onready var dialogues = get_tree().get_root().get_node("World/Dialogues")
var dialogue
#get_parent().get_node("/root/Player/Ball")

#power ups order (list)
#var powerups

#all power ups and their levels
var powerup_speed = 1
var powerup_jump = 1
var powerup_shield = 0
var powerup_rocket = 0
var powerup_slow = 0
var powerup_airjump = 0
var powerup_fly = 0
var powerup_jumpcombo = 0
var powerup_magnesis = 0
var powerup_waterwalking = 0
var powerup_charge = 0
var powerup_sizeup = 0
var powerup_sizedown = 0
var powerup_spikes = 0
var powerup_gravity = 0
var powerup_bounce = 0


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
#func _ready():
#	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func Dialogue(dict):
	
	var DialogueScene = preload("res://scenes/Dialogue.tscn")
	dialogue = DialogueScene.instance()
	var textEngine = dialogue.get_node("SyndiBox")
	var character = dialogue.get_node("Character")
	
	textEngine.DIALOG = dict["text"]
	textEngine.TEXT_VOICE = "res://voice/"+dict["voice"]
	character.animation = dict["animation"]
	
	dialogues.add_child(dialogue)
	#queue_free()
