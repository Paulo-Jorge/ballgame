extends Spatial

#export var powerup_type = "Rocket"
export(String, "Rocket", "Shield", "Jump") var powerup_type
export var rotation_speed = 0.04
#onready var mesh = get_parent().get_node("PowerUp/"+powerup_type)
var mesh
#onready var mesh = get_node(powerup_type)
#onready var sound = get_node("SFX")
onready var sfx = get_parent().get_node("Sound/SFX_POWERUP")

func _ready():
	mesh = get_node(powerup_type)
	mesh.visible = true
	

func _process(delta):
	mesh.rotate_y(rotation_speed)

func _on_Area_body_entered(body):
	if body.name == "Ball":
		### Register in Global
		if powerup_type == "Rocket":
			Global.powerup_rocket += 1
		elif powerup_type == "Shield":
			Global.powerup_shield += 1
			Global.player.get_node("Powers/Shield").visible = true
		elif powerup_type == "Jump":
			Global.powerup_jump += 1
		elif powerup_type == "Speed":
			Global.powerup_speed += 1
		#sfx.play_sound(audiofile)
		sfx.play()
		#sound.play()
		hide()
		#Wait 1 sec before calling dialogue
		yield(get_tree().create_timer(1.0), "timeout")
		Global.Dialogue(Dialogues.dialogues["1"])
		queue_free()
		#hide()
